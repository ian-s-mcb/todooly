const { UPDATE_TODO } = require('./utils/todoQueries')
const sendQuery = require('./utils/sendQuery')

exports.handler = async event => {
  const { id, text, completed } = JSON.parse(event.body)
  const variables = { id, text, completed }

  try {
    const { updateTodo: updatedTodo } = await sendQuery(UPDATE_TODO, variables)

    return {
      statusCode: 200,
      body: JSON.stringify(updatedTodo)
    }
  } catch (err) {
    console.error(err)
    return {
      statusCode: 500,
      body: JSON.stringify({ err: 'Something went wrong' })
    }
  }
}
