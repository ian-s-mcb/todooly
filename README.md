# todooly

A CRUD todo list web app built with React, Netlify Functions
(serverless), FaunaDB, and GraphQL, Bootstrap, and FontAwesome.

### [Demo][demo]

### Screencast

![short screencast of project][screencast]

[demo]: https://todooly.netlify.app/
[screencast]: https://i.imgur.com/d8AleuK.gif
